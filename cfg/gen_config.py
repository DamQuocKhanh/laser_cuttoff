#!/usr/bin/env python

PACKAGE = "laser_cutoff"

from dynamic_reconfigure.parameter_generator_catkin import *

gen = ParameterGenerator()

# Add parameters: 'name', 'type', 'level', 'description', 'default', 'min', 'max
gen.add("min_range", double_t, 0, "Minimum range value", 0.1, 0.0, 10.0)
gen.add("max_range", double_t, 0, "Maximum range value", 5.0, 0.0, 10.0)
gen.add("enable_filter", bool_t, 0, "Enable/Disable the laser filter", True)

# Generate the configuration file
exit(gen.generate(PACKAGE, "laser_cutoff", "LaserFilterConfig"))

