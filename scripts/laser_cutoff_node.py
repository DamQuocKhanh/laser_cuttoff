#!/usr/bin/env python

import rospy
from sensor_msgs.msg import LaserScan
from dynamic_reconfigure.server import Server
from laser_cutoff.cfg import laserCuttoffConfig  # Make sure to replace 'your_package' with your actual package name

class LaserFilter:
    def __init__(self):
        rospy.init_node('laser_filter_node')

        # Dynamic reconfigure server
        self.dynamic_reconfigure_server = Server(laserCuttoffConfig, self.dynamic_reconfigure_callback)

        rospy.Subscriber('/scan_map', LaserScan, self.laser_callback)
        self.filtered_scan_pub = rospy.Publisher('/filtered_scan', LaserScan, queue_size=10)

        self.min_range = 0.1
        self.max_range = 1.5
        self.enable = False

    def dynamic_reconfigure_callback(self, config, level):
        # Update the range values from dynamic reconfigure
        self.min_range = config['min_range']
        self.max_range = config['max_range']
        self.enable = config['enable_filter']
        rospy.loginfo("Updated filter range: min_range={}, max_range={}".format(self.min_range, self.max_range))
        return config

    def laser_callback(self, scan_msg):
        # Create a new LaserScan message for the filtered data
        filtered_scan = LaserScan()
        filtered_scan.header = scan_msg.header
        filtered_scan.angle_min = scan_msg.angle_min
        filtered_scan.angle_max = scan_msg.angle_max
        filtered_scan.angle_increment = scan_msg.angle_increment
        filtered_scan.time_increment = scan_msg.time_increment
        filtered_scan.scan_time = scan_msg.scan_time
        filtered_scan.range_min = self.min_range
        filtered_scan.range_max = self.max_range
        if self.enable:
            # Filter the ranges based on the specified range values
            filtered_scan.ranges = [min(self.max_range, max(self.min_range, r)) for r in scan_msg.ranges]
            filtered_scan.range_min = self.min_range
            filtered_scan.range_max = self.max_range
        else:
            filtered_scan.ranges = scan_msg.ranges
            filtered_scan.range_min = scan_msg.range_min
            filtered_scan.range_max = scan_msg.range_max

        # Publish the filtered LaserScan message
        self.filtered_scan_pub.publish(filtered_scan)

if __name__ == '__main__':
    laser_filter = LaserFilter()
    rospy.spin()
